use amethyst::core::cgmath::Vector3;
use amethyst::ecs::prelude::{Component, DenseVecStorage};

impl Component for RectCollider {
    type Storage = DenseVecStorage<Self>;
}

// v1 bottom left
// v2 bottom right
// v3 top left
// v4 top right
pub struct RectCollider {
    pub height: f32,
    pub width: f32,
    pub offset: [f32;2],
    v1: Vector3<f32>,
    v2: Vector3<f32>,
    v3: Vector3<f32>,
    v4: Vector3<f32>
}

impl RectCollider {
    pub fn new(width: f32, height: f32, offset: [f32;2]) -> RectCollider {

        let v1: Vector3<f32> = Vector3::new(-width/2.0 + offset[0],-height/2.0 + offset[1], 0.0);
        let v2: Vector3<f32> = Vector3::new(width/2.0 + offset[0],-height/2.0 + offset[1], 0.0);
        let v3: Vector3<f32> = Vector3::new(-width/2.0 + offset[0],height/2.0 + offset[1], 0.0);
        let v4: Vector3<f32> = Vector3::new(width/2.0 + offset[0],height/2.0 + offset[1], 0.0);

        RectCollider {
            width,
            height,
            offset,
            v1,
            v2,
            v3,
            v4
        }
    }

    pub fn print_me(&self) {
        println!("Bounding box v1: <{},{}>",self.v1[0],self.v1[1])
    }

    
}


//
//impl RectCollider {
//    pub fn new(v1: Vector3<f32>,v2: Vector3<f32>,v3: Vector3<f32>,v4: Vector3<f32>) -> RectCollider{
//        RectCollider {
//            v1,v2,v3,v4
//        }
//    }
//
//    pub fn newFromArray(v: &mut [Vector3<f32>]) -> RectCollider {
//        RectCollider {
//            v1: v[0],
//            v2: v[1],
//            v3: v[2],
//            v4: v[3],
//        }
//    }
//
//    pub fn updateFromArray(&mut self, v: &mut [Vector3<f32>]) {
//        self.v1 = v[0];
//        self.v2 = v[1];
//        self.v3 = v[2];
//        self.v4 = v[3];
//    }
//
//    fn containsPoint(&self, point: Vector3<f32>) -> bool {
//        if     point[0] >= self.v1[0] && point[1] >= self.v1[1]
//            && point[0] <= self.v2[0] && point[1] >= self.v2[1]
//            && point[0] >= self.v3[0] && point[1] <= self.v3[1]
//            && point[0] <= self.v4[0] && point[1] <= self.v4[1]{
//            return true;
//        } false;
//    }
//
//    fn collidesWithRect(&self, other: RectCollider) -> bool {
//        if     self.containsPoint(other.v1)
//            && self.containsPoint(other.v2)
//            && self.containsPoint(other.v3)
//            && self.containsPoint(other.v4){
//            return true;
//        } false;
//    }
//}