use amethyst::core::cgmath::Vector3;
use amethyst::ecs::prelude::{Component, DenseVecStorage};
use rand::prelude::*;

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}

pub struct Ball {
    pub size: f32,
    pub speed: f32,
    pub velocity: Vector3<f32>
}

impl Ball {
    pub fn new(size: f32, speed: f32) -> Ball {

        let mut rng = rand::thread_rng();
        let angle: f32 = rng.gen_range(0.0, 360.0);
        println!("Starting ball angle: {}",angle.to_string());

        let y_speed = speed * angle.sin();
        let x_speed = speed * angle.cos();
        println!("Starting y speed: {}",y_speed.to_string());
        println!("Starting x speed: {}",x_speed.to_string());

        Ball {
            size,
            speed,
            velocity: Vector3::new(x_speed,y_speed,0.0),
        }
    }

    pub fn update_velocity(&mut self, x: f32, y: f32){
        self.velocity[0] = x;
        self.velocity[1] = y;
    }
}
