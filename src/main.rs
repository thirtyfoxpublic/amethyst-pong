extern crate amethyst;
extern crate rand;

use amethyst::prelude::*;
use amethyst::renderer::{DisplayConfig, DrawSprite, Pipeline,
                         RenderBundle, Stage};
use amethyst::core::transform::TransformBundle;
use amethyst::input::InputBundle;
use pong::Pong;

mod pong;
mod systems;
mod components;

fn main() -> amethyst::Result<()> {
    // We'll put the rest of the code here.
    amethyst::start_logger(Default::default());
    let display_config_path = "./resources/display_config.ron";
    let config = DisplayConfig::load(&display_config_path);
    let binding_config_path= "./resources/bindings_config.ron";
    let input_bundle = InputBundle::<String, String>::new().with_bindings_from_file(binding_config_path)?;
    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.00196, 0.23726, 0.21765, 1.0], 1.0)
            .with_pass(DrawSprite::new()),
    );

    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(input_bundle)?
        .with(systems::PaddleSystem, "paddle_system", &["input_system"])
        .with(systems::BallSystem, "ball_system",&["input_system"]);
    let mut game = Application::new("./", Pong, game_data)?;
    game.run();

    Ok(())
}
