//use amethyst::core::Transform;
//use amethyst::ecs::{Join, System, WriteStorage};
//use amethyst::core::cgmath::Vector3;
//use pong::{ARENA_HEIGHT, ARENA_WIDTH};
//use ::components::{Ball};
//
//pub struct CollisionSystem;
//
//impl<'s> System<'s> for CollisionSystem {
//    type SystemData = (
//        WriteStorage<'s, Transform>,
//        WriteStorage<'s, RectCollider>,
//    );
//
//    fn run(&mut self, (mut transforms, mut rect_colliders): Self::SystemData) {
//        for (mut rect_collider, mut transform) in (&mut rect_colliders, &mut transforms).join() {
//
//            let x = transform.translation[0];
//            let y = transform.translation[1];
//
//            let dx: f32 = ball.velocity[0];
//            let dy: f32 = ball.velocity[1];
//
//            if x > ball.size/2.0 && x < (ARENA_WIDTH - ball.size/2.0) && y > ball.size/2.0 && y < (ARENA_HEIGHT- ball.size/2.0) {
//                // velocity remains the same;
//            } else if x <= ball.size/2.0 || x >= (ARENA_WIDTH - ball.size/2.0) {
//                ball.update_velocity(0.0 - dx , 0.0 + dy);
//            } else if y <= ball.size/2.0 || y >= (ARENA_HEIGHT- ball.size/2.0) {
//                ball.update_velocity(0.0 + dx , 0.0 - dy);
//            }
//
//            transform.translation = transform.translation + Vector3::new(ball.velocity[0], ball.velocity[1], ball.velocity[2]);
//
//
//        }
//    }
//}
